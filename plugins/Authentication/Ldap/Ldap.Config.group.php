<?php
$conf['settings']['host'] = 'ildap.nottingham.ac.uk';
$conf['settings']['port'] = '389';
$conf['settings']['version'] = '3';
$conf['settings']['starttls'] = 'false';
$conf['settings']['binddn'] = 'CN=LDAP_IMAT,CN=Users,DC=intdir,DC=nottingham,DC=ac,DC=uk';
$conf['settings']['bindpw'] = 'LdAp_1matt';
$conf['settings']['basedn'] = 'OU=University,DC=intdir,DC=nottingham,DC=ac,DC=uk';
$conf['settings']['filter'] = '';
$conf['settings']['scope'] = '';
$conf['settings']['required.group'] = 'CN=LS-PPN-BSU,OU=Groups,OU=University,DC=intdir,DC=nottingham,DC=ac,DC=uk';
$conf['settings']['database.auth.when.ldap.user.not.found'] = 'true';
$conf['settings']['ldap.debug.enabled'] = 'true';
$conf['settings']['attribute.mapping'] = 'sn=sn,givenname=givenname,mail=mail,physicaldeliveryofficename=physicaldeliveryofficename,title=title';
$conf['settings']['user.id.attribute'] = 'sAMAccountName';
?>
