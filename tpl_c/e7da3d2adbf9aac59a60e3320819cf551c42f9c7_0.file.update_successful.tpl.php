<?php
/* Smarty version 3.1.30, created on 2017-08-30 14:50:04
  from "/usr/local/lib/bsu/booked/tpl/Ajax/reservation/update_successful.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_59a6c28c753080_41530451',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e7da3d2adbf9aac59a60e3320819cf551c42f9c7' => 
    array (
      0 => '/usr/local/lib/bsu/booked/tpl/Ajax/reservation/update_successful.tpl',
      1 => 1499888928,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:Ajax/reservation/save_successful.tpl' => 1,
  ),
),false)) {
function content_59a6c28c753080_41530451 (Smarty_Internal_Template $_smarty_tpl) {
?>

<?php $_smarty_tpl->_subTemplateRender("file:Ajax/reservation/save_successful.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('divId'=>"reservation-updated",'messageKey'=>"ReservationUpdated"), 0, false);
}
}
