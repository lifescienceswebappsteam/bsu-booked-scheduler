<?php
/* Smarty version 3.1.30, created on 2017-08-29 13:00:16
  from "/usr/local/lib/bsu/booked/lang/en_us/InviteUser.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_59a557501d18c6_42334296',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8672c30fc9c5b6d73fdab63d20d0260c67ae7caa' => 
    array (
      0 => '/usr/local/lib/bsu/booked/lang/en_us/InviteUser.tpl',
      1 => 1499888886,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59a557501d18c6_42334296 (Smarty_Internal_Template $_smarty_tpl) {
?>

<?php echo $_smarty_tpl->tpl_vars['FullName']->value;?>
 has invited you to register for an account with <?php echo $_smarty_tpl->tpl_vars['AppTitle']->value;?>
. Please <a href="<?php echo $_smarty_tpl->tpl_vars['RegisterUrl']->value;?>
">register your account</a>.<?php }
}
