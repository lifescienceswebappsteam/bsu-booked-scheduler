<?php
/* Smarty version 3.1.30, created on 2017-08-29 10:43:55
  from "/usr/local/lib/bsu/booked/tpl/Calendar/calendar.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_59a5375bbc80d2_76980476',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2fa2208cb96eb84cdc02f2c056ab8ff2d7b8c134' => 
    array (
      0 => '/usr/local/lib/bsu/booked/tpl/Calendar/calendar.tpl',
      1 => 1499888928,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:Calendar/calendar-page-base.tpl' => 1,
  ),
),false)) {
function content_59a5375bbc80d2_76980476 (Smarty_Internal_Template $_smarty_tpl) {
?>


<?php ob_start();
echo Pages::CALENDAR;
$_prefixVariable1=ob_get_clean();
$_smarty_tpl->_assignInScope('pageUrl', $_prefixVariable1);
$_smarty_tpl->_assignInScope('pageIdSuffix', "calendar");
$_smarty_tpl->_assignInScope('subscriptionTpl', "calendar.subscription.tpl");
$_smarty_tpl->_subTemplateRender("file:Calendar/calendar-page-base.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
