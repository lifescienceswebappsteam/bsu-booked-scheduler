<?php
/* Smarty version 3.1.30, created on 2017-08-17 15:34:26
  from "/usr/local/lib/bsu/booked/tpl/support-and-credits.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5995a9720ff341_93308648',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4aa64189bede7dd047495a335001095bb14671b3' => 
    array (
      0 => '/usr/local/lib/bsu/booked/tpl/support-and-credits.tpl',
      1 => 1499888930,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:globalheader.tpl' => 1,
    'file:globalfooter.tpl' => 1,
  ),
),false)) {
function content_5995a9720ff341_93308648 (Smarty_Internal_Template $_smarty_tpl) {
?>

<?php $_smarty_tpl->_subTemplateRender("file:globalheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<h1>About Booked Scheduler</h1>

<div id="help">

<h2>Support</h2>

<p><a href="http://www.bookedscheduler.com/">Booked Scheduler Official Project Home</a></p>

<p><a href="http://php.brickhost.com/forums/">Community Support</a></p>

<p><a href="https://sourceforge.net/projects/Booked Scheduler/">Booked Scheduler SourceForge Project Home</a></p>

<h2>Credits</h2>

<h3>Authors</h3>

<p>Nick Korbel</p>

<p>Dung Le</p>

<p>Jan Mattila</p>

<p>Paul Menchini</p>


<h3>Translators</h3>

<p>Boris Vatin (French)</p>
<p>Dariusz Kliszewski, Grzegorz Bis (Polish)</p>
<p>Tadafumi Kouzato (Japanese)</p>
<p>Jonne Olie (Dutch)</p>
<p>Julio Guedimin, Manuel J. Morgado Morano, Laura Arjona (Spanish)</p>
<p>Jordi Divins (Catalan)</p>
<p>Nicola Ruggero, Daniele Cordella, Marco Ponti (Italian)</p>
<p>Olli Räisänen, Afaf Fafa (Finnish)</p>
<p>Jakub Baláš, Leoš Jedlička (Czech)</p>
<p>Maik Standtke, Sven de Vries, Jonas Endersch (German)</p>
<p>Stephen Höglund (Swedish)</p>
<p>Vladislav Zhivkov (Bulgarian)</p>
<p>Bart Verheyde (Flemisch)</p>
<p>William Oliveira, Fábio Luiz Barbosa (Portuguese Brazil)</p>
<p>Yosef Branse (Hebrew)</p>
<p>Vladislav Zhivkov (Bulgarian)</p>
<p>Linas Redeckis (Lithuanian)</p>
<p>Davor Tomasevic (Croatian)</p>
<p>Alenka Kavčič (Slovenian)</p>
<p>Tage Jørgensen  (Danish)</p>
<p>Tran Dai Nghia (Vietnamese)</p>
<p>Karl Jaani (Estonian)</p>
<p>Erhan Harmankaya (Turkish)</p>
<p>Txeli Sanchez (Basque)</p>

<h3>Thank you to the following projects and libraries</h3>

<p>Smarty</p>

<p>PEAR</p>

<p>adLDAP</p>

<p>jQuery</p>

<p>FullCalendar</p>

<p>log4php</p>

<p>securimage</p>

<p>SimpleImage</p>

<p>PHPMailer</p>

<p>jsTimezoneDetect</p>

<p>jqplot</p>

<p>FeedWriter</p>

<p>Slim</p>

<h2>License</h2>

<p>Booked Scheduler free and open source, licenced under the GNU GENERAL PUBLIC LICENSE. Please see the included
	License file for more details.</p>

<?php $_smarty_tpl->_subTemplateRender("file:globalfooter.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
